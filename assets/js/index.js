function myFunction() {
  var x = document.querySelector(".navbar-responsive-a");
  if (x.className === "navbar-responsive-a") {
    x.className += " responsive";
  } else {
    x.className = "navbar-responsive-a";
  }
}

document.getElementById("btn-submit").addEventListener("click", (event) => {
  event.preventDefault();
  let errors = [];
  console.log("Clicked!");
  const fullName = document.querySelector("#name").value;
  const email = document.querySelector("#email").value;
  const phoneNumber = document.querySelector("#phone").value;
  const review = document.querySelector("#review").value;
  const terms = document.querySelector("#checkboxTerms").checked;

  if (!fullName) {
    toastr.error("Nu ati introdus numele");
    errors.push({ text: "Nu ati introdus numele", el: fullName });
  }
  if (!email) {
    toastr.error("Nu ati introdus emailul!");
    errors.push({ text: "Nu ati introdus emailul", el: email });
  }
  if (!phoneNumber) {
    toastr.error("Nu ati introdus numarul de telefon");
    errors.push({
      text: "Nu ati introdus numarul de telefon",
      el: phoneNumber,
    });
  }
  if (!review) {
    toastr.error("Nu ati introdus review ul");
    errors.push({ text: "Nu ati introdus review ul", el: phoneNumber });
  }

  if (terms === false) {
    toastr.error("Trebuie sa fiti de acord cu termenii si conditiile");
    errors.push({
      text: "Trebuie sa fiti de acord cu termenii si conditiile",
      el: terms,
    });
  }
  if (errors.length === 0) {
    let form = document.querySelector("#form-final");
    toastr.success("Datele au fost introduse cu succes!", "Succes");
  }
});
